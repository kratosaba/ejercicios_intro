def es_primo(N):
    if N == 1 or N == 2:
        return True

    lista_divisble = range(2, N)
    for e in lista_divisble:
        h = N % e
        if h == 0:
            primo = False
            break
        else:
            primo = True

    return primo


def es_bisiesto(N):
    if N%4 == 0 and N%100==0:
        bisiesto = True
    elif N%100 == 0 :
        bisiesto = False
    elif N%4 == 0 :
        bisiesto = True
    return bisiesto